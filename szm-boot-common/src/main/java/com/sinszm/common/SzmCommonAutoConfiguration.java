package com.sinszm.common;


import com.google.gson.Gson;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

/**
 * Szm框架启动配置
 *
 * @author chenjianbo
 */
@ComponentScans({
        @ComponentScan("com.sinszm.*"),
        @ComponentScan("cn.hutool.extra.spring")
})
public class SzmCommonAutoConfiguration {

    @ConditionalOnMissingBean(
            name = "gson",
            value = Gson.class
    )
    @Bean
    public Gson gson() {
        return new Gson()
                .newBuilder().create();
    }

}
