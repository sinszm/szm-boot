package com.sinszm.common.exception;

import org.springframework.util.StringUtils;

/**
 * 错误代码通用接口
 *
 * @author chenjianbo
 */
public interface ApiError {

    /**
     * 错误代码
     * @return 代码
     */
    String getCode();

    /**
     * 错误消息
     * @return 消息
     */
    String getMessage();

    /**
     * 组装错误消息
     * @return  错误代码信息
     */
    default String message() {
        String code = getCode();
        if (StringUtils.isEmpty(code)) {
            code = SystemApiError.SYSTEM_ERROR_02.name();
        }
        String message = getMessage();
        if (StringUtils.isEmpty(message)) {
            message = SystemApiError.SYSTEM_ERROR_02.getMessage();
        }
        return code.toUpperCase()
                .replace("_", "-")
                + ":" + message;
    }

}
