package com.sinszm.common.exception;

/**
 * 系统错误代码
 *
 * @author chenjianbo
 */
public enum SystemApiError implements ApiError {

    SYSTEM_ERROR_01("网络连接超时，请稍后重试！"),

    SYSTEM_ERROR_02("未知异常"),

    SYSTEM_ERROR_03("SQL异常"),

    SIGN_ERROR_01("签名参数不能为空"),

    SIGN_ERROR_02("签名密钥不能为空"),

    SIGN_ERROR_03("请指定签名算法方式"),

    SIGN_ERROR_04("签名算法异常"),

    XML_ERROR_01("XML数据为空"),

    XML_ERROR_02("XML输入数据异常"),

    XML_ERROR_03("XML输出数据异常"),

    XML_ERROR_04("XML输出文件地址不合法");

    private String message;

    SystemApiError(String message) {
        this.message = message;
    }

    @Override
    public String getCode() {
        return this.name();
    }

    @Override
    public String getMessage() {
        return message;
    }

}
