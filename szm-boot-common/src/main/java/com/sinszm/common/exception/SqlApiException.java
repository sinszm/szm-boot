package com.sinszm.common.exception;

/**
 * 用于SQL异常的定义类型
 *
 * @author chenjianbo
 */
public class SqlApiException extends ApiException {

    public SqlApiException() {
        super(SystemApiError.SYSTEM_ERROR_03);
    }

}
