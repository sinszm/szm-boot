package com.sinszm.common;

import cn.hutool.extra.spring.SpringUtil;
import org.springframework.context.ApplicationContext;

/**
 * 实例读取器
 *
 * @author chenjianbo
 */
public final class SpringContext {

    private ApplicationContext context = null;

    private static class Spring {
        private static final SpringContext HOLDER = new SpringContext();
    }

    private SpringContext() {
    }

    public static SpringContext instance() {
        return Spring.HOLDER;
    }

    void init() {
        if (this.context == null) {
            this.context = SpringUtil.getApplicationContext();
        }
    }

    public ApplicationContext context() {
        init();
        return this.context;
    }

    public <T> T getBean(Class<T> clazz){
        return context().getBean(clazz);
    }

    public Object getBean(String name){
        return context().getBean(name);
    }

    public <T> T getBean(String name,Class<T> clazz){
        return context().getBean(name, clazz);
    }

    public String activeProfile() {
        return context().getEnvironment().getActiveProfiles()[0];
    }

}