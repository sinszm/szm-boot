package com.sinszm.common;

import com.sinszm.common.exception.ApiError;
import com.sinszm.common.exception.SystemApiError;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.nutz.json.Json;
import org.nutz.json.JsonFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Date;

/**
 * 统一返回值对象
 *
 * @author chenjianbo
 */
@ApiModel(value = "请求结果", description = "请求结果")
public class Response<T> implements Serializable {

    private static final long serialVersionUID = 1638622422783383442L;

    private static final String MESSAGE = "SUCCESS";

    private static final Logger log = LoggerFactory.getLogger(Response.class);

    /**
     * 状态
     */
    @ApiModelProperty(value = "结果状态 [true成功;false失败]", dataType = "Boolean", required = true, example = "true")
    private Boolean state;

    /**
     * 请求消息内容
     */
    @ApiModelProperty(value = "结果描述", dataType = "String", required = true, example = MESSAGE)
    private String message;

    /**
     * 返回值对象
     */
    @ApiModelProperty(value = "结果数据", dataType = "Object", required = true)
    private T body;

    /**
     * 异常栈
     */
    @ApiModelProperty(value = "异常栈信息", dataType = "Throwable")
    private Throwable cause;

    private Response() {
    }

    public Boolean getState() {
        return state;
    }

    private void setState(Boolean state) {
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    private void setMessage(String message) {
        this.message = message;
    }

    public T getBody() {
        return body;
    }

    private void setBody(T body) {
        this.body = body;
    }

    public Throwable getCause() {
        return cause;
    }

    private void setCause(Throwable cause) {
        this.cause = cause;
    }

    /**
     * 成功返回
     * @param body  返回值
     * @param <T>   类型
     * @return      响应
     */
    public synchronized static <T> Response<T> success(T body) {
        Response<T> response = new Response<>();
        response.setState(true);
        response.setMessage(MESSAGE);
        if (body != null) {
            response.setBody(body);
        }
        response.setCause(null);
        return response;
    }

    private static String formatTime2Millisecond(Date date) {
        return String.format("%tF %tT.%tL", date, date, date);
    }

    /**
     * 失败返回
     * @param apiError  错误代码
     * @param cause     异常栈
     * @return          响应
     */
    public synchronized static Response<Object> fail(ApiError apiError, Throwable cause) {
        Response<Object> response = new Response<>();
        response.setState(false);
        response.setMessage(
                apiError.message() == null ? SystemApiError.SYSTEM_ERROR_01.message() : apiError.message()
        );
        response.setCause(null);
        response.setBody(null);
        if (cause != null) {
            log.error("异常时间: {}, 异常信息: {} -> {}", formatTime2Millisecond(new Date()),Json.toJson(response, JsonFormat.tidy()), cause);
        } else {
            log.error("异常时间: {}, 异常信息: {}", formatTime2Millisecond(new Date()),Json.toJson(response, JsonFormat.tidy()));
        }
        return response;
    }

    /**
     * 自定义失败信息组装
     * @param body  数据
     * @param msg   消息
     * @param <T>   泛型定义
     * @return      响应
     */
    @Deprecated
    public synchronized static <T> Response<T> fail(T body, String msg) {
        Response<T> response = new Response<>();
        response.setState(false);
        response.setMessage(msg == null ? SystemApiError.SYSTEM_ERROR_01.message() : msg);
        response.setCause(null);
        response.setBody(body);
        return response;
    }

    public synchronized static Response<String> fail(String msg) {
        Response<String> response = new Response<>();
        response.setState(false);
        response.setMessage(msg == null ? SystemApiError.SYSTEM_ERROR_01.message() : msg);
        response.setCause(null);
        response.setBody("");
        return response;
    }

    @Override
    public String toString() {
        return Json.toJson(this, JsonFormat.tidy());
    }
}
