package com.sinszm.common.util;


import com.sinszm.common.exception.ApiException;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * 图片合成工具类
 *
 * @author sinszm
 */
public class PictureSynthesisUtil {

    /**
     * 合成图片
     * @param in0       背景图片
     * @param in1       浮层图片
     * @param in1Width  浮层图片宽度
     * @param in1Height 浮层图片高度
     * @param x         X轴定位
     * @param y         Y轴定位
     * @return          已合成成功的图片流
     */
    public static InputStream synthesis(
            InputStream in0,
            InputStream in1,
            Integer in1Width,
            Integer in1Height,
            int x,
            int y) {
        if (in0 == null || in1 == null) {
            throw new ApiException("待合成图片流不能为空.");
        }
        try {
            return synthesis(ImageIO.read(in0), ImageIO.read(in1), in1Width, in1Height, x, y);
        } catch (IOException e) {
            throw new ApiException("图片合成异常.");
        }
    }

    /**
     * 合成图片
     * @param in0       背景图片
     * @param in1       浮层图片
     * @param x         X轴定位
     * @param y         Y轴定位
     * @return          已合成成功的图片流
     */
    public static InputStream synthesis(
            InputStream in0,
            InputStream in1,
            int x,
            int y) {
        return synthesis(in0, in1, null, null, x, y);
    }

    /**
     * 合成图片
     * @param in0Bi     背景图片
     * @param in1Bi     浮层图片
     * @param in1Width  浮层图片宽度
     * @param in1Height 浮层图片高度
     * @param x         X轴定位
     * @param y         Y轴定位
     * @return          已合成成功的图片流
     */
    public static InputStream synthesis(
            BufferedImage in0Bi,
            BufferedImage in1Bi,
            Integer in1Width,
            Integer in1Height,
            int x,
            int y) {
        if (in0Bi == null || in1Bi == null) {
            throw new ApiException("待合成图片流不能为空.");
        }
        try {
            //图片合并
            Graphics2D backG = in0Bi.createGraphics();
            backG.drawImage(
                    in1Bi,
                    x,
                    y,
                    in1Width == null ? in1Bi.getWidth() : in1Width,
                    in1Height == null ? in1Bi.getHeight() : in1Height,
                    null);
            backG.dispose();
            //文件流转换
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(in0Bi, "png", os);
            return new ByteArrayInputStream(os.toByteArray());
        } catch (Exception e) {
            throw new ApiException("图片合成异常.");
        }
    }

}
