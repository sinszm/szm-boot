# SZM-BOOT生态

#### 介绍
快速构建基于SpringBoot的微服务及相关服务生态  Copyright © 2019 sinsz.com All rights reserved. https://www.sinsz.com

#### 软件架构
本项目为微服务依赖组件构建集市，便于程序员、架构师快速搭建项目所需结构和通用处理框架，整体设计思路在于组件解耦、零配置。


#### 安装教程
1.依赖引入
```
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.sinszm</groupId>
                <artifactId>szm-boot</artifactId>
                <version>0.0.6</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
    
    <dependencies>
        <!--公共组件-->
        <dependency>
            <groupId>com.sinszm</groupId>
            <artifactId>szm-boot-common</artifactId>
        </dependency>

        <!--Web及WebSocket支持组件-->
        <dependency>
            <groupId>com.sinszm</groupId>
            <artifactId>szm-boot-web</artifactId>
        </dependency>

        <!--Mybatis数据操作支持组件-->
        <dependency>
            <groupId>com.sinszm</groupId>
            <artifactId>szm-boot-mdb</artifactId>
        </dependency>
        
        <!--微信支付基础组件-->
        <dependency>
            <groupId>com.sinszm</groupId>
            <artifactId>szm-boot-basicwx</artifactId>
        </dependency>

        <!--SpringBoot的测试依赖-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

```

2.添加配置
```
-- szm-boot-web
#如不需要WebSocket支持，请将ws开关设置为false
szm.boot.ws.enable=true
szm.boot.ws.uri=/xxx,/xxxx
szm.boot.ws.js-uri=/js/xxx,/js/xxxx
szm.boot.ws.processor=......

-- szm-boot-mdb
#1.必须要添加数据源配置参数，并且需要在项目的配置类处开启扫描【`@MapperScan("xxx.xxx.xx")`】
#2.在resources目录创建目录`mybatis`或`META-INF/mybatis`目录，该目录中主要存储对应的mapper数据库查询配置文件
spring.datasource.platform=mysql
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.datasource.url=jdbc:mysql://192.168.8.203:3306/gftech_data
spring.datasource.username=admin
spring.datasource.password=admin
spring.datasource.hikari.connection-test-query='select 1'

-- szm-boot-basicwx 
支付配置
szm:
  boot:
    wx:
      app-id: wx9bd8fa49xxxxx
      mch-key: 30B9C650BF2CC0B776C80D3061FD1AFC
      #服务商商户
      mch-id: 14234232342
      #服务商商户对应的证书
      mch-cert-uri: /Users/chenjianbo/cert/14xxxxxxrt/apiclient_cert.p12
      #普通商户
      #mch-id: 15081231231
      #普通商户对应的证书
      #mch-cert-uri: /Users/chenjianbo/cert/150xxxxxx0213_cert/apiclient_cert.p12

```

#### 使用说明

1. 示例1：
```
    @GetMapping("/hello")
    public Response<Map<String, String>> hello() {
        Map<String,String> r = new HashMap<>(0);
        r.put("hello","我的中国");
        throw new ApiException(SystemApiError.SYSTEM_ERROR_002);
    }
```

2. 示例2：
```
    @PostMapping("/hello2")
    public Response<Map<String, String>> hello2(HttpServletRequest request) {
        Map<String,String> r = new HashMap<>(0);
        r.put("hello",requestBody(request));
        return Response.success(r);
    }
```

3. 示例3：
```
@Component
public class Xim implements ImProcessor {

    @Override
    public void open(WebSocketSession session) {
        System.out.println("开始推送离线消息" + session.getAttributes());
    }

    @Override
    public void message(WebSocketSession session, BinaryMessage message) {
        try {
            ImBuf.Im im = ImBuf.Im.parseFrom(message.getPayload().array());
            System.out.println(im.getBody());
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void closed(WebSocketSession session, CloseStatus closeStatus) {
        System.out.println("执行了关闭" + session.isOpen());
    }
}
```

4. 示例4
```
//mapper类
public interface VersionMapper {

    List<Map<String, String>> selectAll();

}
//mapper类对应的mapper数据操作配置文件
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.sinsz.sockettest.mapper.VersionMapper">

    <select id="selectAll" resultType="java.util.Map">
        select * from gftech_version
    </select>

</mapper>

//方法调用
    /**
     * 分页查询示例
     */
    @GetMapping("/resp")
    public Response<PageInfo<List<Map<String, String>>>> responseInfo() {
        PageInfo<List<Map<String, String>>> page = PageHelper.startPage(1,2)
                .doSelectPageInfo(() -> versionMapper.selectAll());
        return Response.success(page);
    }
```

5. 支付示例
```
@GetMapping("/create")
    public Response<Object> createPay(HttpServletRequest request) {
        UnifiedOrder order = new UnifiedOrder();
        order.setOpenid("oIaRgxPPQn6D5EURHeArhB3tO2QA");
        order.setBody("支付测试");
        order.setDetail("测试");
        order.setAttach("name=123");
        order.setOutTradeNo(UUID.randomUUID().toString().replace("-",""));
        order.setTotalFee(1);
        order.setSpbillCreateIp(request.getRemoteAddr());
        order.setTradeType("JSAPI");
        order.setNotifyUrl("https://www.guanfangyiliao.com");
        /**服务商时使用*/
        order.setSubAppid("xxxxxxxxxx");
        order.setSubMchId("xxxxxxx");
        CommonUtils.logger("支付预下单").info(Json.toJson(order));
        UnifiedOrderResp resp = payService.unifiedOrder(order);
        //支付签名
        Map<String, String> map = new HashMap<>(0);
        map.put("appId", resp.getAppid());
        map.put("timeStamp", (System.currentTimeMillis()/1000) + "");
        map.put("nonceStr", WXPayUtil.generateNonceStr());
        map.put("package", "prepay_id="+ resp.getPrepayId());
        map.put("signType", payService.getSignType().getName());
        try {
            map.put("paySign", WXPayUtil.generateSignature(map, payService.getConfig().getKey(), payService.getSignType()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.success(map);
    }

    @GetMapping("/query")
    public Response<Object> queryOrder(String orderId) {
        OrderQuery orderQuery = new OrderQuery();
        orderQuery.setOutTradeNo(orderId);
        /**服务商时使用*/
        orderQuery.setSubAppid("xxxxxxxxxx");
        orderQuery.setSubMchId("xxxxxxx");
        return Response.success(
                payService.orderQuery(orderQuery)
        );
    }

    @GetMapping("/close")
    public Response<Object> closeOrder(String orderId) {
        CloseOrder closeOrder = new CloseOrder();
        closeOrder.setOutTradeNo(orderId);
        /**服务商时使用*/
        closeOrder.setSubAppid("xxxxxxxxxx");
        closeOrder.setSubMchId("xxxxxxx");
        return Response.success(
                payService.closeOrder(closeOrder)
        );
    }

    @GetMapping("/bill")
    public Response<Object> downloadBill() {
        BillOrder billOrder = new BillOrder();
        billOrder.setBillType("ALL");
        billOrder.setBillDate("20200409");
        /**服务商时使用*/
        billOrder.setSubAppid("xxxxxxxxxx");
        billOrder.setSubMchId("xxxxxxx");
        return Response.success(
                payService.downloadBill(billOrder)
        );
    }

    /**
     * 服务商模式退款需要单独获取授权
     * @return
     */
    @GetMapping("/refund")
    public Response<Object> refund(String orderId) {
        RefundOrder refundOrder = new RefundOrder();
        refundOrder.setOutTradeNo(orderId);
        refundOrder.setOutRefundNo(UUID.randomUUID().toString().replace("-",""));
        refundOrder.setTotalFee(1);
        refundOrder.setRefundFee(1);
        /**服务商时使用*/
        refundOrder.setSubAppid("xxxxxxxxxx");
        refundOrder.setSubMchId("xxxxxxx");
        CommonUtils.logger("退款参数").info(Json.toJson(refundOrder));
        return Response.success(
                payService.refund(refundOrder)
        );
    }

    /**
     * 服务商模式退款需要单独获取授权
     * @return
     */
    @GetMapping("/refundQuery")
    public Response<Object> refundQuery(String refundId) {
        OrderRefundQuery query = new OrderRefundQuery();
        query.setOutRefundNo(refundId);
        /**服务商时使用*/
        query.setSubAppid("xxxxxxxxxx");
        query.setSubMchId("xxxxxxx");
        return Response.success(
                payService.refundQuery(query)
        );
    }
```


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request