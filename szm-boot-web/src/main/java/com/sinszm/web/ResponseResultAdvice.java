package com.sinszm.web;

import com.sinszm.web.annotation.Response;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.lang.annotation.Annotation;

/**
 * 正确结果的统一返回
 *
 * @author sinszm
 */
@RestControllerAdvice
public class ResponseResultAdvice implements ResponseBodyAdvice<Object> {

    private static final Class<? extends Annotation> ANNOTATION_TYPE = Response.class;

    @Override
    public boolean supports(MethodParameter parameter, Class<? extends HttpMessageConverter<?>> clz) {
        return AnnotatedElementUtils.hasAnnotation(parameter.getContainingClass(), ANNOTATION_TYPE)
                || parameter.hasMethodAnnotation(ANNOTATION_TYPE);
    }

    @Override
    public Object beforeBodyWrite(Object body,
                                  MethodParameter parameter,
                                  MediaType type,
                                  Class<? extends HttpMessageConverter<?>> clz,
                                  ServerHttpRequest request,
                                  ServerHttpResponse response) {
        if (body instanceof com.sinszm.common.Response) {
            return body;
        }
        com.sinszm.common.Response<Object> result = com.sinszm.common.Response.success(body);
        if (body instanceof String) {
            return result.toString();
        }
        return result;
    }

}
