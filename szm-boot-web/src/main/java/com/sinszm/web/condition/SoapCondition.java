package com.sinszm.web.condition;

import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
import org.springframework.boot.autoconfigure.condition.SpringBootCondition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * Soap开关条件
 *
 * @author chenjianbo
 */
public class SoapCondition extends SpringBootCondition {

    private static final String ENABLE = "szm.boot.soap.enable";

    @Override
    public ConditionOutcome getMatchOutcome(ConditionContext context, AnnotatedTypeMetadata metadata) {
        Environment environment = context.getEnvironment();
        Boolean enable = environment.getProperty(ENABLE, Boolean.class);
        if (enable == null || !enable) {
            return new ConditionOutcome(false, "@Soap");
        }
        return new ConditionOutcome(true, "@Soap");
    }

}
