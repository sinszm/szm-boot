package com.sinszm.web.condition;

import com.sinszm.common.exception.ApiException;
import com.sinszm.web.support.WebError;
import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
import org.springframework.boot.autoconfigure.condition.SpringBootCondition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * WebSocket条件触发器
 *
 * @author chenjianbo
 */
public class WsCondition extends SpringBootCondition {

    private static final String WS_ENABLE = "szm.boot.ws.enable";
    private static final String WS_URI = "szm.boot.ws.uri";

    @Override
    public ConditionOutcome getMatchOutcome(ConditionContext context, AnnotatedTypeMetadata metadata) {
        Boolean enable = context.getEnvironment()
                .getProperty(WS_ENABLE, Boolean.class);
        if (ObjectUtils.isEmpty(enable)) {
            return new ConditionOutcome(false, "@Ws");
        }
        List uri = context.getEnvironment().getProperty(WS_URI, List.class);
        if (enable && ObjectUtils.isEmpty(uri)) {
            throw new ApiException(WebError.WEB_ERROR_001);
        }
        return new ConditionOutcome(enable, "@Ws");
    }

}
