package com.sinszm.web.auth0;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * JWT Payload
 *
 * @author sinszm
 */
public class JwtPayload implements Serializable {

    private static final long serialVersionUID = -8721961672263744120L;

    /**
     * 签发人/作者
     */
    private String issuer;

    /**
     * 签发时间/创建时间
     */
    private Date issuedAt;

    /**
     * 用户ID
     */
    private String uid;

    /**
     * 过期时间,单位：毫秒
     */
    private long expiresAt;

    /**
     * 签名唯一编码
     */
    private String jwtId;

    /**
     * 用户自定义数据
     */
    private Map<String, ?> data;

    /** ********************************** */
    /**
     * 签发人/作者
     */
    private String iss;

    /**
     * 签发时间/创建时间
     */
    private long iat;

    /**
     * 用户ID
     */
    private String aud;

    /**
     * 过期时间,单位：毫秒
     */
    private long exp;

    /**
     * 签名唯一编码
     */
    private String jti;
    /** ********************************** */

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public Date getIssuedAt() {
        return issuedAt;
    }

    public void setIssuedAt(Date issuedAt) {
        this.issuedAt = issuedAt;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public long getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(long expiresAt) {
        this.expiresAt = expiresAt;
    }

    public String getJwtId() {
        return jwtId;
    }

    public void setJwtId(String jwtId) {
        this.jwtId = jwtId;
    }

    public Map<String, ?> getData() {
        return data;
    }

    public void setData(Map<String, ?> data) {
        this.data = data;
    }

    public String getIss() {
        return iss;
    }

    /**
     * 设置签发者
     * @param iss   参数
     */
    public void setIss(String iss) {
        this.iss = iss;
        this.issuer = this.iss;
    }

    public long getIat() {
        return iat;
    }

    /**
     * 设置签名时间
     * @param iat   参数
     */
    public void setIat(long iat) {
        this.iat = iat;
        this.issuedAt = new Date(this.iat * 1000);
    }

    public String getAud() {
        return aud;
    }

    /**
     * 设置用户ID
     * @param aud   参数
     */
    public void setAud(String aud) {
        this.aud = aud;
        this.uid = this.aud;
    }

    public long getExp() {
        return exp;
    }

    /**
     * 设置剩余过期时间
     * @param exp   参数
     */
    public void setExp(long exp) {
        this.exp = exp;
        this.expiresAt = this.exp * 1000 - System.currentTimeMillis();
    }

    public String getJti() {
        return jti;
    }

    /**
     * 设置主键ID
     * @param jti   参数
     */
    public void setJti(String jti) {
        this.jti = jti;
        this.jwtId = this.jti;
    }
}
