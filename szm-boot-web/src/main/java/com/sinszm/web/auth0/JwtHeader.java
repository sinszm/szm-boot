package com.sinszm.web.auth0;

import java.io.Serializable;

/**
 * JWT Header
 *
 * @author sinszm
 */
public class JwtHeader implements Serializable {

    private static final long serialVersionUID = -2968108168033833963L;

    /**
     * 客户端ID，可为空
     */
    private String clientId;

    /**
     * 其他数据
     */
    private Object data;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
