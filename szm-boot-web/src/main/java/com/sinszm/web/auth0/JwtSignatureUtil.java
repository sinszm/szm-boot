package com.sinszm.web.auth0;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.sinszm.common.exception.ApiException;
import com.sinszm.common.util.CommonUtils;

import java.util.Date;

/**
 * JWT签名工具
 *
 * @author sinszm
 */
public final class JwtSignatureUtil {

    /**
     * 创建Token
     *
     * @param header        数据头
     * @param payload       数据体
     * @param secretKey     签名密钥
     * @return              Token串
     */
    public synchronized static String createToken(JwtHeader header, JwtPayload payload, String secretKey) {
        if (header == null || CommonUtils.isEmpty(header.getClientId())) {
            throw new ApiException("请求头或请求头客户端ID不能为空");
        }
        if (payload == null) {
            throw new ApiException("装载数据不能为空");
        }
        //当前时间
        Date currentTime = payload.getIssuedAt() == null ? new Date() : payload.getIssuedAt();
        long expires = payload.getExpiresAt() <= 0 ? 86400000 : payload.getExpiresAt();
        if (CommonUtils.isEmpty(payload.getUid())) {
            throw new ApiException("装载数据uid不能为空");
        }
        if (CommonUtils.isEmpty(secretKey)) {
            throw new ApiException("签名密钥不能为空");
        }
        return JWT.create()
                .withHeader(BeanUtil.beanToMap(header))
                .withIssuer(CommonUtils.isEmpty(payload.getIssuer()) ? "guest" : StrUtil.trimToEmpty(payload.getIssuer()))
                .withIssuedAt(currentTime)
                .withExpiresAt(new Date(currentTime.getTime() + expires))
                .withAudience(StrUtil.trimToEmpty(payload.getUid()))
                .withJWTId(CommonUtils.isEmpty(payload.getJwtId())? StrUtil.uuid() : StrUtil.trimToEmpty(payload.getJwtId()))
                .withClaim("data", payload.getData())
                .sign(Algorithm.HMAC256(StrUtil.trimToEmpty(secretKey)));
    }

    /**
     * 创建Authorization
     *
     * @param header        数据头
     * @param payload       数据体
     * @param secretKey     签名密钥
     * @return              Token串
     */
    public synchronized static String createAuthorization(JwtHeader header, JwtPayload payload, String secretKey) {
        return "Bearer " + createToken(header, payload, secretKey);
    }

    /**
     * 验证Token或Authorization是否有效
     *
     * @param token         token 或 Authorization
     * @param secretKey     签名密钥
     * @return              验证结果
     */
    public synchronized static boolean verify(String token, String secretKey) {
        if (CommonUtils.isEmpty(token)) {
            throw new ApiException("加密数据串不能为空");
        }
        if (CommonUtils.isEmpty(secretKey)) {
            throw new ApiException("签名密钥不能为空");
        }
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(StrUtil.trimToEmpty(secretKey))).build();
        try {
            String verifierToken = StrUtil.startWith(token, "Bearer ")
                    ? StrUtil.subAfter(token, "Bearer ", false) : token;
            verifier.verify(verifierToken);
            return true;
        } catch (JWTVerificationException e) {
            return false;
        }
    }

    /**
     * 读取Header
     *
     * @param token         token 或 Authorization
     * @param secretKey     签名密钥
     * @return              验证结果
     */
    public synchronized static JwtHeader getHeader(String token, String secretKey) {
        if (verify(token, secretKey)) {
            try {
                String verifierToken = StrUtil.startWith(token, "Bearer ")
                        ? StrUtil.subAfter(token, "Bearer ", false) : token;
                String header = JWT.decode(verifierToken).getHeader();
                String data = Base64.decodeStr(header);
                return JSONUtil.toBean(data, JwtHeader.class);
            } catch (JWTDecodeException e) {
                e.printStackTrace(System.err);
            }
        }
        return null;
    }

    /**
     * 读取Payload
     *
     * @param token         token 或 Authorization
     * @param secretKey     签名密钥
     * @return              验证结果
     */
    public synchronized static JwtPayload getPayload(String token, String secretKey) {
        if (verify(token, secretKey)) {
            try {
                String verifierToken = StrUtil.startWith(token, "Bearer ")
                        ? StrUtil.subAfter(token, "Bearer ", false) : token;
                String payload = JWT.decode(verifierToken).getPayload();
                String data = Base64.decodeStr(payload);
                return JSONUtil.toBean(data, JwtPayload.class);
            } catch (JWTDecodeException e) {
                e.printStackTrace(System.err);
            }
        }
        return null;
    }

}
