package com.sinszm.web.support;

import com.sinszm.common.exception.ApiError;

/**
 * Web的异常代码
 *
 * @author chenjianbo
 */
public enum WebError implements ApiError {

    WEB_ERROR_001("WS缺失URI配置"),

    WEB_ERROR_002("WS二进制编码错误"),

    WEB_ERROR_003("WS缺失消息处理插槽"),

    WEB_ERROR_004("WS消息处理插槽未实例化"),

    WEB_ERROR_005("WS未能获取通道信息"),

    WEB_ERROR_006("WS异常中断"),
    ;

    private String message;

    WebError(String message) {
        this.message = message;
    }

    @Override
    public String getCode() {
        return this.name();
    }

    @Override
    public String getMessage() {
        return message;
    }

}