package com.sinszm.web.support;

import com.google.common.base.Predicate;

/**
 * 扩展Doc包加载
 *
 * @author sinszm
 */
public class DocPrediate<T> implements Predicate<T> {

    private final java.util.function.Predicate<T> predicate;

    public DocPrediate(java.util.function.Predicate<T> predicate) {
        this.predicate = predicate;
    }

    @Override
    public boolean apply(T input) {
        return true;
    }

    @Override
    public boolean test(T input) {
        return this.apply(input);
    }

    @Override
    public java.util.function.Predicate<T> and(java.util.function.Predicate<? super T> other) {
        return this.predicate.and(other);
    }

    @Override
    public java.util.function.Predicate<T> negate() {
        return this.predicate.negate();
    }

    @Override
    public java.util.function.Predicate<T> or(java.util.function.Predicate<? super T> other) {
        return this.predicate.or(other);
    }
}
