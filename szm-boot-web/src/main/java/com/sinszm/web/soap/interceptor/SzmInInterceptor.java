package com.sinszm.web.soap.interceptor;

import com.sinszm.common.util.CommonUtils;
import com.sinszm.web.soap.SzmLocalTime;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 标题：访问进入日志
 * 描述：
 * 类名：SzmInInterceptor
 * 时间：2020-6-20 14:17
 *
 * @author chenjianbo
 */
public class SzmInInterceptor extends AbstractSoapInterceptor {

    private static final Logger log = LoggerFactory.getLogger(SzmInInterceptor.class);

    public SzmInInterceptor() {
        super(Phase.PRE_INVOKE);
    }

    @Override
    public void handleMessage(SoapMessage message) throws Fault {
        String threadNumber = CommonUtils.getProcessId() + "/" + CommonUtils.getThreadId();
        //存储当前进入时间
        SzmLocalTime.getInstance().put(System.currentTimeMillis());
        //日志打印
        log.info(">>[{}]>> WebService Started >>", threadNumber);
    }

}
