package com.sinszm.web.soap.service;

import com.sinszm.web.soap.interceptor.SzmInInterceptor;
import com.sinszm.web.soap.interceptor.SzmOutInterceptor;
import org.apache.cxf.interceptor.InInterceptors;
import org.apache.cxf.interceptor.OutInterceptors;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.BindingType;

import static javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_MTOM_BINDING;

/**
 * 标题：HL7的WebService服务
 * 描述：
 * 类名：SzmHipSoapService
 * 时间：2020-6-20 10:51
 *
 * @author chenjianbo
 */
@BindingType(SOAP12HTTP_MTOM_BINDING)
@InInterceptors(classes = SzmInInterceptor.class)
@OutInterceptors(classes = SzmOutInterceptor.class)
@WebService(
        serviceName = "HIPMessageServer",
        targetNamespace = "urn:hl7-org:v3",
        portName = "HIPMessageServerPort",
        endpointInterface = "com.sinszm.web.soap.service.SzmHipSoapService"
)
public interface SzmHipSoapService {

    /**
     * 业务使用
     * @param action    方法名
     * @param message   消息体
     * @return          响应数据
     */
    @WebMethod(operationName = "HIPMessageServer", action = "urn:hl7-org:v3/HIPMessageServer")
    @WebResult(partName = "HIPMessageServerResponse", name = "HIPMessageServerResult")
    String request(
            @WebParam(name = "action") String action,
            @WebParam(name = "message") String message
    );

}
