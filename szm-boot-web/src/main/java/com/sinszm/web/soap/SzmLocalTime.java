package com.sinszm.web.soap;

import org.springframework.core.NamedInheritableThreadLocal;

import java.util.Optional;

/**
 * 当前时间缓存
 *
 * @author sinszm
 */
public class SzmLocalTime {

    private final NamedInheritableThreadLocal<Long> local = new NamedInheritableThreadLocal<>(this.getClass().getSimpleName());

    private static final SzmLocalTime LOCAL_TIME = new SzmLocalTime();

    private SzmLocalTime() {
    }

    public static SzmLocalTime getInstance() {
        return LOCAL_TIME;
    }

    /**
     * 存储时间
     * @param currentTime   当前时间
     */
    public void put(long currentTime) {
        local.set(currentTime);
    }

    /**
     * 获取时间
     * @return  时间戳
     */
    public long get() {
        long currentTime = Optional.ofNullable(local.get()).orElse(Long.MAX_VALUE);
        local.remove();
        return currentTime;
    }

}
