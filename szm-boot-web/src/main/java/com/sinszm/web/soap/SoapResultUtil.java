package com.sinszm.web.soap;

import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.XmlUtil;
import org.nutz.json.Json;

/**
 * 返回数据对象工具
 *
 * @author sinszm
 */
public final class SoapResultUtil {

    private static final String ROOT_NAME = "node";

    private static final String XML_HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>";

    enum Status {
        /**
         * 成功
         */
        OK,
        /**
         * 失败
         */
        FAIL
    }

    /**
     * 成功
     * @param data  数据
     * @return      xml
     */
    public static String ok(Object data) {
        SoapResult result = new SoapResult(Status.OK.name(), Status.OK.name(), data);
        return XmlUtil.mapToXmlStr(Json.fromJsonAsMap(Object.class, result.toString()), ROOT_NAME)
                .replace(XML_HEAD, "");
    }

    /**
     * 失败
     * @param errMsg    错误信息
     * @return          xml
     */
    public static String fail(String errMsg) {
        SoapResult result = new SoapResult(Status.FAIL.name(), StrUtil.trimToEmpty(errMsg), null);
        return XmlUtil.mapToXmlStr(Json.fromJsonAsMap(Object.class, result.toString()), ROOT_NAME)
                .replace(XML_HEAD, "");
    }

}
