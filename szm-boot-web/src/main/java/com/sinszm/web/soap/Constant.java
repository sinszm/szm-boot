package com.sinszm.web.soap;

/**
 * 标题：Soap常量
 * 描述：
 * 类名：Constant
 * 时间：2020-6-20 10:08
 *
 * @author chenjianbo
 */
public final class Constant {

    /**
     * 命名空间
     */
    public static final String NAMESPACE = "https://www.sinsz.com/";

    public static final String SERVER_NAME = "SoapService";

    public static final String SERVER_INTERFACE = "com.sinszm.web.soap.service.SzmSoapService";

    /**
     * 默认服务根路径
     */
    public static final String DEFAULT_ROOT_PATH = "/sws/*";

    public static final String SPRING_SERVLET = "cxfServlet";

    public static final String DEFAULT_API_NAME = "/service";

    public static final String SOAP_CURRENT_TIME = "_soap_request_tag";

}
