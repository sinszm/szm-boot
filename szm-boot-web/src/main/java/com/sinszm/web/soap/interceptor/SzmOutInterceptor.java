package com.sinszm.web.soap.interceptor;

import com.sinszm.common.util.CommonUtils;
import com.sinszm.web.soap.SzmLocalTime;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 标题：访问结束日志
 * 描述：
 * 类名：SzmOutInterceptor
 * 时间：2020-6-20 14:17
 *
 * @author chenjianbo
 */
public class SzmOutInterceptor extends AbstractSoapInterceptor {

    private static final Logger log = LoggerFactory.getLogger(SzmOutInterceptor.class);

    public SzmOutInterceptor() {
        super(Phase.SEND_ENDING);
    }

    @Override
    public void handleMessage(SoapMessage message) throws Fault {
        long ms = System.currentTimeMillis() - SzmLocalTime.getInstance().get();
        String threadNumber = CommonUtils.getProcessId() + "/" + CommonUtils.getThreadId();
        log.info(">>[{}]>>[耗时：{} ms]>> WebService End >>",threadNumber, ms);
    }

}
