package com.sinszm.web.soap.service;

import com.sinszm.web.soap.interceptor.SzmInInterceptor;
import com.sinszm.web.soap.interceptor.SzmOutInterceptor;
import org.apache.cxf.interceptor.InInterceptors;
import org.apache.cxf.interceptor.OutInterceptors;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import static com.sinszm.web.soap.Constant.*;

/**
 * 标题：默认的WebService服务
 * 描述：
 * 类名：SzmSoapService
 * 时间：2020-6-20 10:51
 *
 * @author chenjianbo
 */
@InInterceptors(classes = SzmInInterceptor.class)
@OutInterceptors(classes = SzmOutInterceptor.class)
@WebService(
        serviceName = SERVER_NAME,
        targetNamespace = NAMESPACE,
        endpointInterface = SERVER_INTERFACE
)
public interface SzmSoapService {

    /**
     * 请求
     * @param body  请求内容
     * @param time  请求签名的时间，格式：20200101121212
     * @param sign  签名
     *              利用：sign = MD5(body + time + 【签名密钥】)
     * @return      响应数据
     */
    @WebMethod
    @WebResult(name = "response")
    String request(
            @WebParam(name = "body") String body,
            @WebParam(name = "time") String time,
            @WebParam(name = "sign") String sign
    );

    /**
     * 请求
     * @param attribute     附加属性
     * @param body          请求内容
     * @param time          请求签名的时间，格式：20200101121212
     * @param sign          签名
     *                      利用：sign = MD5(attribute + body + time + 【签名密钥】)
     * @return              响应数据
     */
    @WebMethod
    @WebResult(name = "response")
    String requestExtend(
            @WebParam(name = "attribute") String attribute,
            @WebParam(name = "body") String body,
            @WebParam(name = "time") String time,
            @WebParam(name = "sign") String sign
    );

}
