package com.sinszm.web.soap.service;

import com.sinszm.web.soap.SoapResultUtil;

/**
 * 标题：对外暴露的扩展插槽
 * 描述：
 * 类名：WsApi
 * 时间：2020-6-20 11:10
 *
 * @author chenjianbo
 */
public interface SoapApi {

    /**
     * 请求
     * @param body  请求内容
     * @param time  请求签名的时间，格式：20200101121212
     * @param sign  签名
     *              利用：sign = MD5(body + time + 【签名密钥】)
     * @return      响应数据
     */
    default String request(String body, String time, String sign){
        return SoapResultUtil.fail("业务未实现");
    }

    /**
     * 请求
     * @param attribute     附加属性
     * @param body          请求内容
     * @param time          请求签名的时间，格式：20200101121212
     * @param sign          签名
     *                      利用：sign = MD5(attribute + body + time + 【签名密钥】)
     * @return              响应数据
     */
    default String requestExtend(String attribute,String body,String time,String sign) {
        return SoapResultUtil.fail("业务未实现");
    }

    /**
     * 特定的业务
     * @param action    自定义方法
     * @param message   自定义消息体
     * @return          结果
     */
    default String request(String action, String message) {
        return "";
    }

}
