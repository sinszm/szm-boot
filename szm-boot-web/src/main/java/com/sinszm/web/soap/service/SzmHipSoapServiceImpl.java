package com.sinszm.web.soap.service;

import cn.hutool.core.util.StrUtil;
import com.sinszm.common.SpringContext;
import com.sinszm.web.soap.SzmSoapProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.BindingType;

import static javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_MTOM_BINDING;

/**
 * HL7的特定业务实现
 *
 * @author sinszm
 */
@WebService(targetNamespace = "urn:hl7-org:v3")
@BindingType(SOAP12HTTP_MTOM_BINDING)
@Service
public class SzmHipSoapServiceImpl implements SzmHipSoapService {

    private static final Logger log = LoggerFactory.getLogger(SzmHipSoapServiceImpl.class);

    @Resource
    private SzmSoapProperties szmSoapProperties;

    @Override
    public String request(String action, String message) {
        if (!szmSoapProperties.isEnable()) {
            log.error("服务未启用");
            return "";
        }
        if (szmSoapProperties.getService() == null) {
            log.error("未配置服务实现扩展");
            return "";
        }
        try {
            SoapApi soapApi = SpringContext.instance().getBean(szmSoapProperties.getService());
            return soapApi.request(StrUtil.trimToEmpty(action), StrUtil.trimToEmpty(message));
        } catch (Exception e) {
            e.printStackTrace(System.err);
            log.error("服务调用异常 ~ ", e.getCause());
        }
        return "";
    }

}
