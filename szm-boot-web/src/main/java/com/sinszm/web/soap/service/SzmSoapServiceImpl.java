package com.sinszm.web.soap.service;

import com.sinszm.common.SpringContext;
import com.sinszm.common.util.CommonUtils;
import com.sinszm.web.soap.SoapResultUtil;
import com.sinszm.web.soap.SzmSoapProperties;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.jws.WebService;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static com.sinszm.web.soap.Constant.NAMESPACE;

/**
 * 标题：默认的WebService服务
 * 描述：
 * 类名：SzmSoapServiceImpl
 * 时间：2020-6-20 10:54
 *
 * @author chenjianbo
 */
@WebService(targetNamespace = NAMESPACE)
@Service
public class SzmSoapServiceImpl implements SzmSoapService {

    @Resource
    private SzmSoapProperties szmSoapProperties;

    private String format(String date) {
        if (CommonUtils.isEmpty(date)
                || !CommonUtils.isNumber(date)
                || date.length() != 14) {
            return null;
        }
        try {
            new SimpleDateFormat("yyyyMMddHHmmss").parse(date);
            return date;
        } catch (ParseException ignored) {
            return null;
        }
    }

    private String propertiesValidate() {
        if (!szmSoapProperties.isEnable()) {
            return SoapResultUtil.fail("未启用服务");
        }
        if (szmSoapProperties.getService() == null) {
            return SoapResultUtil.fail("未配置服务实现扩展");
        }
        return null;
    }

    /**
     * 请求
     * @param body  请求内容
     * @param time  请求签名的时间，格式：20200101121212
     * @param sign  签名
     *              利用：sign = MD5(body + time + 【签名密钥】)
     * @return      响应数据
     */
    @Override
    public String request(String body, String time, String sign) {
        String tips = propertiesValidate();
        if (tips != null) {
            return tips;
        }
        //当设置有签名密钥时，需要对结果进行签名验证
        if (!CommonUtils.isEmpty(szmSoapProperties.getCipher())) {
            if (CommonUtils.isEmpty(time)) {
                return SoapResultUtil.fail("签名时间不能为空");
            }
            if (format(time) == null) {
                return SoapResultUtil.fail("签名时间格式错误,建议采用 “20200101121212” 格式");
            }
            if (CommonUtils.isEmpty(sign)) {
                return SoapResultUtil.fail("数据签名不能为空");
            }
            if (!sign.equals(CommonUtils.MD5(
                    StringUtils.trimToEmpty(body) + time + szmSoapProperties.getCipher()
            ))) {
                return SoapResultUtil.fail("签名验证未通过");
            }
        }
        try {
            SoapApi soapApi = SpringContext.instance().getBean(szmSoapProperties.getService());
            return soapApi.request(
                    StringUtils.trimToEmpty(body),
                    StringUtils.trimToEmpty(time),
                    StringUtils.trimToEmpty(sign)
            );
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
        return SoapResultUtil.fail("服务实现扩展配置错误");
    }

    /**
     * 请求
     * @param attribute     附加属性
     * @param body          请求内容
     * @param time          请求签名的时间，格式：20200101121212
     * @param sign          签名
     *                      利用：sign = MD5(attribute + body + time + 【签名密钥】)
     * @return              响应数据
     */
    @Override
    public String requestExtend(String attribute, String body, String time, String sign) {
        String tips = propertiesValidate();
        if (tips != null) {
            return tips;
        }
        //当设置有签名密钥时，需要对结果进行签名验证
        if (!CommonUtils.isEmpty(szmSoapProperties.getCipher())) {
            if (CommonUtils.isEmpty(time)) {
                return SoapResultUtil.fail("签名时间不能为空");
            }
            if (format(time) == null) {
                return SoapResultUtil.fail("签名时间格式错误,建议采用 “20200101121212” 格式");
            }
            if (CommonUtils.isEmpty(sign)) {
                return SoapResultUtil.fail("数据签名不能为空");
            }
            if (!sign.equals(CommonUtils.MD5(
                    StringUtils.trimToEmpty(attribute) + StringUtils.trimToEmpty(body) + time + szmSoapProperties.getCipher()
            ))) {
                return SoapResultUtil.fail("签名验证未通过");
            }
        }
        try {
            SoapApi soapApi = SpringContext.instance().getBean(szmSoapProperties.getService());
            return soapApi.requestExtend(
                    StringUtils.trimToEmpty(attribute),
                    StringUtils.trimToEmpty(body),
                    StringUtils.trimToEmpty(time),
                    StringUtils.trimToEmpty(sign)
            );
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
        return SoapResultUtil.fail("服务实现扩展配置错误");
    }
}
