package com.sinszm.web.soap;

import org.nutz.json.Json;
import org.nutz.json.JsonFormat;

/**
 * 返回数据对象
 *
 * @author sinszm
 */
public class SoapResult {

    /**
     * 错误代码，成功为OK
     */
    private String code;

    /**
     * 代码描述内容
     */
    private String message;

    /**
     * 数据体
     */
    private Object data;

    public SoapResult(String code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    @Override
    public String toString() {
        return Json.toJson(this, JsonFormat.tidy());
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
