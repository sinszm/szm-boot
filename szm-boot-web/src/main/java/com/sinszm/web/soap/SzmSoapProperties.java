package com.sinszm.web.soap;

import com.sinszm.web.soap.service.SoapApi;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 标题：Soap配置文件
 * 描述：
 * 类名：SzmSoapProperties
 * 时间：2020-6-20 10:13
 *
 * @author chenjianbo
 */
@ConfigurationProperties("szm.boot.soap")
public class SzmSoapProperties {

    /**
     * 是否开启WebService服务
     */
    private boolean enable;

    /**
     * 服务根路径配置
     */
    private String path;

    /**
     * 服务实现的接口扩展
     */
    private Class<? extends SoapApi> service;

    /**
     * 签名密钥
     */
    private String cipher;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Class<? extends SoapApi> getService() {
        return service;
    }

    public void setService(Class<? extends SoapApi> service) {
        this.service = service;
    }

    public String getCipher() {
        return cipher;
    }

    public void setCipher(String cipher) {
        this.cipher = cipher;
    }
}
