package com.sinszm.web.properties;

import cn.hutool.core.util.StrUtil;
import org.springframework.boot.context.properties.ConfigurationProperties;
import springfox.documentation.service.Contact;

/**
 * 接口文档Ui配置
 *
 * @author chenjianbo
 */
@ConfigurationProperties(value = "szm.boot.swagger")
public class SwaggerProperties {

    /**
     * 是否启用
     * false: 不启用
     * true: 启用
     */
    private Boolean enable = false;

    /**
     * 文档标题
     */
    private String title = "接口文档";

    /**
     * 版本号
     */
    private String version = "v1.0";

    /**
     * 访问起始路径
     */
    private String basePath;

    /**
     * 文档前缀地址
     */
    private String documentPrefixUri;

    /**
     * 描述信息
     */
    private String description = "暂无描述信息";

    /**
     * 联系信息
     */
    private Contact contact = new Contact("博学浮生", "https://www.sinsz.com", "admin@sinsz.com");

    /**
     * licenseUrl
     */
    private String licenseUrl = "https://www.sinsz.com/LICENSE";

    /**
     * 获取处理过后的前缀地址
     *
     * @return 地址
     */
    public String swaggerBasePath() {
        String prefix = StrUtil.trimToEmpty(this.getBasePath());
        prefix = StrUtil.startWith(prefix, "/") ? prefix : "/" + prefix;
        prefix = prefix.length() > 1 && StrUtil.endWith(prefix, "/")
                ? StrUtil.sub(prefix, 0, prefix.length() -1) : prefix;
        return prefix;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public String getLicenseUrl() {
        return licenseUrl;
    }

    public void setLicenseUrl(String licenseUrl) {
        this.licenseUrl = licenseUrl;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public String getDocumentPrefixUri() {
        return documentPrefixUri;
    }

    public void setDocumentPrefixUri(String documentPrefixUri) {
        this.documentPrefixUri = documentPrefixUri;
    }
}
