package com.sinszm.web.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * Web基础配置
 *
 * @author chenjianbo
 */
@ConfigurationProperties(value = "szm.boot.base")
public class WebProperties {

    /**
     * 需要拦截的路径集合，为空时默认拦截全部
     */
    private List<String> patterns;

    /**
     * 排除需要拦截的路径集合规则
     */
    private List<String> excludePatterns;

    public List<String> getPatterns() {
        return patterns;
    }

    public void setPatterns(List<String> patterns) {
        this.patterns = patterns;
    }

    public List<String> getExcludePatterns() {
        return excludePatterns;
    }

    public void setExcludePatterns(List<String> excludePatterns) {
        this.excludePatterns = excludePatterns;
    }
}
