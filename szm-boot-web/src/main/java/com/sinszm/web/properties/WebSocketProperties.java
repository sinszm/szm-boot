package com.sinszm.web.properties;

import com.sinszm.web.socket.ImProcessor;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * WebSocket配置
 *
 * @author chenjianbo
 */
@ConfigurationProperties(value = "szm.boot.ws")
public class WebSocketProperties {

    /**
     * 是否开启ws
     */
    private Boolean enable;

    /**
     * 暴露的ws地址
     */
    private List<String> uri;

    /**
     * 暴露用于SockJs连接的地址
     */
    private List<String> jsUri;

    /**
     * 即时通信处理器插槽
     */
    private Class<? extends ImProcessor> processor;

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public List<String> getUri() {
        return uri;
    }

    public void setUri(List<String> uri) {
        this.uri = uri;
    }

    public Class<? extends ImProcessor> getProcessor() {
        return processor;
    }

    public void setProcessor(Class<? extends ImProcessor> processor) {
        this.processor = processor;
    }

    public List<String> getJsUri() {
        return jsUri;
    }

    public void setJsUri(List<String> jsUri) {
        this.jsUri = jsUri;
    }
}
