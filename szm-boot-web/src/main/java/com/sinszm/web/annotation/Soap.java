package com.sinszm.web.annotation;

import com.sinszm.web.condition.SoapCondition;
import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

/**
 * 是否开启Soap服务
 *
 * @author chenjianbo
 */
@Target({ ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(SoapCondition.class)
public @interface Soap {
}
