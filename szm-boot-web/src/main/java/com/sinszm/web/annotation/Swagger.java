package com.sinszm.web.annotation;

import com.sinszm.web.condition.SwaggerCondition;
import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

/**
 * SwaggerUi限定条件
 *
 * @author chenjianbo
 */
@Target({ ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(SwaggerCondition.class)
public @interface Swagger {
}
