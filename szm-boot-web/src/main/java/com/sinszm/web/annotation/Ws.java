package com.sinszm.web.annotation;

import com.sinszm.web.condition.WsCondition;
import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

/**
 * WebSocket条件触发器注解
 *
 * @author chenjianbo
 */
@Target({ ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(WsCondition.class)
public @interface Ws {
}
