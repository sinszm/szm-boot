package com.sinszm.web.cos;


import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 文件存储服务器配置
 * <p>
 *     腾讯云COS
 * </p>
 * @author sinszm
 */
@ConfigurationProperties("szm.boot.cos")
public class CosProperties {

    /**
     * 是否启用
     */
    private boolean enable = false;

    /**
     * 授权ID
     */
    private String secretId;

    /**
     * 授权密钥
     */
    private String secretKey;

    /**
     * 所属地区
     */
    private String region;

    /**
     * 存储桶名称
     */
    private String bucketName;

    /**
     * 线程池大小
     */
    private int threadPool = 1;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getSecretId() {
        return secretId;
    }

    public void setSecretId(String secretId) {
        this.secretId = secretId;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public int getThreadPool() {
        return threadPool;
    }

    public void setThreadPool(int threadPool) {
        this.threadPool = threadPool;
    }
}
