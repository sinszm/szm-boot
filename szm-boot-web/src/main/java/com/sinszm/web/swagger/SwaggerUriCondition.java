package com.sinszm.web.swagger;

import cn.hutool.core.util.StrUtil;
import com.sinszm.common.util.CommonUtils;
import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
import org.springframework.boot.autoconfigure.condition.SpringBootCondition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * 是否启用简略地址文档访问
 *
 * @author chenjianbo
 */
public class SwaggerUriCondition extends SpringBootCondition {

    private static final String URI = "szm.boot.swagger.document-prefix-uri";
    private static final String HTTPS = "https://";
    private static final String HTTP = "http://";

    @Override
    public ConditionOutcome getMatchOutcome(ConditionContext context, AnnotatedTypeMetadata metadata) {
        String url = context.getEnvironment().getProperty(URI, String.class);
        if (CommonUtils.isEmpty(url)) {
            return new ConditionOutcome(false, "模式禁用");
        }
        if (!StrUtil.startWith(StrUtil.trimToEmpty(url).toLowerCase(), HTTPS)
                && !StrUtil.startWith(StrUtil.trimToEmpty(url).toLowerCase(), HTTP)
        ) {
            return new ConditionOutcome(false, "模式禁用");
        }
        return new ConditionOutcome(true, "模式启用");
    }

}
