package com.sinszm.web.swagger;

import cn.hutool.core.util.StrUtil;
import com.sinszm.web.properties.SwaggerProperties;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;

/**
 * 文档简略访问
 *
 * @author sinszm
 */
@SwaggerUri
@Controller
public class SwaggerThemePage {

    @Resource
    private SwaggerProperties swaggerProperties;

    private String url() {
        String prefixUri = swaggerProperties.getDocumentPrefixUri();
        prefixUri = StrUtil.trimToEmpty(prefixUri);
        prefixUri = StrUtil.endWith(prefixUri, "/")
                ? StrUtil.sub(prefixUri, 0, prefixUri.length() -1) : prefixUri;
        return prefixUri + ("/".equals(swaggerProperties.swaggerBasePath()) ? "" : swaggerProperties.swaggerBasePath());
    }

    @GetMapping("/themeDoc")
    public String base() {
        return String.format("redirect:%s/doc.html", StrUtil.trimToEmpty(this.url()));
    }

    @GetMapping("/themeThink")
    public String think() {
        return String.format("redirect:%s/thinkswagger/index.html", StrUtil.trimToEmpty(this.url()));
    }

}
