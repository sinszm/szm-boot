package com.sinszm.web.swagger;

import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

/**
 * 是否启用简略地址文档访问
 *
 * @author chenjianbo
 */
@Target({ ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(SwaggerUriCondition.class)
public @interface SwaggerUri {
}
