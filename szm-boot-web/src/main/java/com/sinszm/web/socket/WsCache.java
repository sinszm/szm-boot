package com.sinszm.web.socket;

import org.springframework.lang.NonNull;
import org.springframework.web.socket.WebSocketSession;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * WS连接缓存工具
 *
 * @author chenjianbo
 */
class WsCache {
    private static WsCache ourInstance = new WsCache();

    static WsCache getInstance() {
        return ourInstance;
    }

    private WsCache() {
    }

    private Map<String, WebSocketSession> sessionMap = new ConcurrentHashMap<>();

    /**
     * 存储缓存
     * @param id        通道ID
     * @param session   通道
     */
    void put(@NonNull String id, @NonNull WebSocketSession session) {
        sessionMap.put(id, session);
    }

    /**
     * 获取缓存对象
     * @param id        通道ID
     */
    WebSocketSession get(@NonNull String id) {
        WebSocketSession wss = sessionMap.get(id);
        if (wss != null && wss.isOpen()) {
            return wss;
        } else {
            clean(id);
        }
        return null;
    }

    /**
     * 清除指定缓存
     * @param id        通道ID
     */
    void clean(@NonNull String id) {
        sessionMap.remove(id);
    }

}
