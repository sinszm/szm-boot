package com.sinszm.web.socket;

import com.sinszm.common.exception.ApiError;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;

/**
 * 即时通信处理器
 *
 * @author chenjianbo
 */
public interface ImProcessor {

    /**
     * 连接成功时执行
     * <p>
     *     可用于处理离线消息
     * </p>
     * @param session   通道
     */
    void open(WebSocketSession session);

    /**
     * 接收到消息时执行
     * @param session   通道
     * @param message   消息
     */
    void message(WebSocketSession session, BinaryMessage message);

    /**
     * 关闭连接时执行
     * @param session       通道
     * @param closeStatus   关闭消息
     */
    void closed(WebSocketSession session, CloseStatus closeStatus);

    /**
     * 发送错误信息
     * @param session   通道
     * @param status    状态码
     * @param error     错误代码
     */
    static void sendError(WebSocketSession session, CloseStatus status, ApiError error) {
        try {
            session.close(status.withReason(error.message()));
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

}
