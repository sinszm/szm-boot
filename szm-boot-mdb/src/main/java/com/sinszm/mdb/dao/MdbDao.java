package com.sinszm.mdb.dao;

import cn.hutool.db.Db;
import cn.hutool.db.Session;
import com.sinszm.mdb.annotation.Mdb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

/**
 * 数据Dao
 *
 * @author sinszm
 */
@Mdb
@ConditionalOnExpression(
        "'${spring.datasource.platform:}'.equalsIgnoreCase('mysql')||'${spring.datasource.platform:}'.equalsIgnoreCase('oracle')"
)
@Component
public class MdbDao {

    private final Db mDb;

    private final Session mDbs;

    @Autowired
    public MdbDao(@Qualifier("mDb") Db mDb, @Qualifier("mDbs") Session mDbs) {
        this.mDb = mDb;
        this.mDbs = mDbs;
    }

    /**
     * 数据操作方法
     * 可自行扩展操作业务
     * @return  实例
     */
    public Db db() {
        return mDb;
    }

    /**
     * 数据操作方法
     * 可自行扩展操作业务
     * @return  实例
     */
    public Session session() {
        return mDbs;
    }

}
