package com.sinszm.mdb.pagebar.annotation;

import java.lang.annotation.*;

/**
 * 用于定于列表表头字段信息和属性
 *
 * @author chenjianbo
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Th {

    /**
     * 列位置编号
     * @return 数字
     */
    int index();

    /**
     * 表头列名
     */
    String value() default "";

    /**
     * SQL中对应的字段名称
     */
    String dbName() default "";

    /**
     * 是否固定列
     * <p>
     *     true:    固定；
     *     false:   可变；
     *     特别备注：固定表示该列必须显示，不可隐藏该列。
     * </p>
     */
    boolean fixed() default true;

    /**
     * 是否可以动态排序
     * <p>
     *     true:    可以动态排序；
     *     false:   不可排序；
     *     如果为true表示列头上要显示排序箭头。
     * </p>
     */
    boolean sorted() default false;

}
