package com.sinszm.mdb.pagebar.annotation;

import com.sinszm.mdb.pagebar.RvType;

import java.lang.annotation.*;

/**
 * 用于验证参数合法性
 *
 * @author chenjianbo
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Rv {

    /**
     * 1.是否必须验证
     *
     * false: 不验证;
     * true : 需要验证;
     *
     * @return 状态
     */
    boolean required() default true;

    /**
     * 2.是否可以为空
     *
     * true:是；
     * false:否；
     *
     * @return 状态
     */
    boolean nullable() default true;

    /**
     * 3.验证类型集合
     *
     * @return 按类型进行验证
     */
    RvType[] types() default {};

    /**
     * RvType.LENGTH
     * 最小长度
     *
     * @return  长度
     */
    int minLength() default 0;

    /**
     * RvType.LENGTH
     * 最大长度
     *
     * @return  长度
     */
    int maxLength() default Integer.MAX_VALUE;

    /**
     * RvType.REGEX
     * 正则匹配规则
     *
     * @return 规则
     */
    String regex() default "";

    /**
     * RvType.TIME
     * 时间格式
     *
     * @return 格式
     */
    String dateFormat() default "yyyy-MM-dd HH:mm:ss";

}
