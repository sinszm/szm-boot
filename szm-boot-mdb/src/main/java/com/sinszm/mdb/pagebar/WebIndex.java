package com.sinszm.mdb.pagebar;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sinszm.mdb.pagebar.annotation.Th;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Transient;

/**
 * 公共序号字段
 *
 * @author chenjianbo
 */
@ApiModel(value = "列序号信息")
@JsonIgnoreProperties("handler")
public class WebIndex {

    /**
     * 序号
     */
    @ApiModelProperty(value = "序号", dataType = "Long", example = "1")
    @Transient
    @Th(value = "序号", index = 1)
    public long index;

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }
}
