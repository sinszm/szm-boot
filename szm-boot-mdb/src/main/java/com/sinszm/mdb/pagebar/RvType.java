package com.sinszm.mdb.pagebar;

/**
 * 验证类型
 *
 * @author chenjianbo
 */
public enum RvType {

    /**
     * 长度验证
     */
    LENGTH,

    /**
     * 正则验证
     */
    REGEX,

    /**
     * 时间格式验证
     */
    TIME

}
