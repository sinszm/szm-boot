package com.sinszm.mdb.pagebar;


import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.ISelect;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sinszm.common.exception.ApiException;
import com.sinszm.common.util.CommonUtils;
import com.sinszm.mdb.pagebar.annotation.Th;
import org.nutz.json.Json;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.sinszm.common.util.CommonUtils.consumerWithIndex;
import static com.sinszm.common.util.CommonUtils.logger;

/**
 * Web分页组件调用工具
 *
 * @author chenjianbo
 */
public final class WebPageUtils {

    private static final Logger logger = LoggerFactory.getLogger(WebPageUtils.class);

    /**
     * 分页组件调用方法
     * @param select    查询器
     * @param request   请求参数
     * @param tClass    泛型类Class
     * @param <T>       泛型
     * @return          响应数据
     */
    public static <T extends WebIndex> WebPageResponse<T> doPage(ISelect select, WebPageRequest request, Class<T> tClass) {
        if (request == null) {
            throw new ApiException("PAGE-RESPONSE-ERR-001","request不能为空");
        }
        request.checkAllParameters();
        PageInfo<T> pageInfo;
        if (StringUtils.isEmpty(request.getField())
                || StringUtils.isEmpty(request.getField().trim())
                || StringUtils.isEmpty(request.getSorted())
                || StringUtils.isEmpty(request.getSorted().trim())
        ) {
            pageInfo = PageHelper.startPage(
                    request.getCurrentNum(),
                    request.getPageSize()
            ).doSelectPageInfo(select);
        } else {
            String sorted = "ASC".equalsIgnoreCase(request.getSorted()) ? "ASC" : "DESC";
            String field = StrUtil.trimToEmpty(request.getField());
            List<String> fields = Stream.of(tClass.getFields())
                    .map(tField -> {
                        Th th = tField.getAnnotation(Th.class);
                        if (th == null) {
                            return null;
                        }
                        return th.sorted()? (StringUtils.isEmpty(th.dbName())? tField.getName() : th.dbName()) : tField.getName();
                    })
                    .filter(Objects::nonNull)
                    .filter(str -> !StrUtil.isEmpty(str) && !"index".equals(str))
                    .collect(Collectors.toList());
            if (fields.contains(field) || isMatchField(field)) {
                logger.info("排序成功进行：{}", field);
                pageInfo = PageHelper.startPage(
                        request.getCurrentNum(),
                        request.getPageSize(),
                        field + " " + sorted
                ).doSelectPageInfo(select);
            } else {
                logger.info("未能进行排序：{}", field);
                pageInfo = PageHelper.startPage(
                        request.getCurrentNum(),
                        request.getPageSize()
                ).doSelectPageInfo(select);
            }
        }
        //格式化返回响应值
        WebPageResponse<T> response = new WebPageResponse<>(tClass);
        response.setList(pageInfo.getList());
        response.setPages(pageInfo.getPages());
        response.setTotal(pageInfo.getTotal());
        response.setPageNum(pageInfo.getPageNum());
        response.setPageSize(pageInfo.getPageSize());
        response.setPrePage(pageInfo.getPrePage());
        response.setNextPage(pageInfo.getNextPage());
        //自动计算序号
        response.getList().forEach(consumerWithIndex((data, index) -> data.setIndex(calculateIndex(response, index))));
        return response;
    }

    private static boolean isMatchField(String content) {
        content = StrUtil.trimToEmpty(content);
        final String spaceChar = " ";
        if (content.contains(spaceChar)) {
            content = StrUtil.sub(content, 0, content.indexOf(" "));
        }
        boolean isMatch = ReUtil.isMatch("^[0-9]*$", content);
        if (isMatch) {
            return false;
        }
        content = StrUtil.replace(content, " ", "");
        //处理开头字符串校验
        isMatch = ReUtil.isMatch("^[a-zA-z].*", content);
        if (!isMatch) {
            return false;
        }
        isMatch = ReUtil.isMatch("^[a-zA-z].*", StrUtil.subSuf(content, content.lastIndexOf(".")+1));
        if (!isMatch) {
            return false;
        }
        return ReUtil.isMatch("^[A-Za-z0-9_.]+$", content);
    }

    /**
     * 返回单据数据
     *
     * @param select    查询器
     * @param request   请求参数
     * @param tClass    泛型类Class
     * @param <T>       泛型
     * @return          响应数据
     */
    public static <T extends WebIndex> WebPageResponse.Bill<T> doPageBill(ISelect select, WebPageRequest request, Class<T> tClass) {
        WebPageResponse<T> response = doPage(select, request, tClass);
        return new WebPageResponse.Bill<>(
                response.getHeaders(),
                response.getList()
        );
    }

    /**
     * 计算序号
     * @param response  响应数据
     * @param index     当前数据循环页中的下标
     * @param <T>       泛型定义
     * @return 序号
     */
    private static <T extends WebIndex> long calculateIndex(WebPageResponse<T> response, Integer index) {
        int currentIndex = (index == null ? 0 : index) + 1;
        return response.getPageNum() <= 1 ? currentIndex : currentIndex + (response.getPageNum() - 1) * response.getPageSize();
    }

}