package com.sinszm.mdb.annotation;

import com.sinszm.common.exception.SqlApiException;
import com.sinszm.mdb.support.MdbConstant;
import org.springframework.transaction.annotation.Transactional;

import java.lang.annotation.*;

/**
 * MDB的事务封装注解
 *
 * @author chenjianbo
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Transactional(rollbackFor = SqlApiException.class, transactionManager=MdbConstant.TRANSACTION_MANAGER)
public @interface MdbTransactional {
}
