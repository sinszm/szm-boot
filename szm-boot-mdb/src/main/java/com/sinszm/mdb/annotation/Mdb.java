package com.sinszm.mdb.annotation;

import com.sinszm.mdb.condition.MdbCondition;
import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

/**
 * 数据连接池参数配置条件触发器
 *
 * @author chenjianbo
 */
@Target({ ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(MdbCondition.class)
public @interface Mdb {
}