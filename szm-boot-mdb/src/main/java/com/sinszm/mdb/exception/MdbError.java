package com.sinszm.mdb.exception;

import com.sinszm.common.exception.ApiError;

/**
 * MDB错误代码
 *
 * @author chenjianbo
 */
public enum MdbError implements ApiError {

    MDB_ERROR_001("数据库配置缺省值"),

    ;

    private String message;

    MdbError(String message) {
        this.message = message;
    }

    @Override
    public String getCode() {
        return this.name();
    }

    @Override
    public String getMessage() {
        return message;
    }
}
