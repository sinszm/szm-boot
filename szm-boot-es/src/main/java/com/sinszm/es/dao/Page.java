package com.sinszm.es.dao;

import java.util.List;

/**
 * 页码信息的数据返回
 *
 * @author sinszm
 */
public class Page<T> {

    /**
     * 每页数量
     */
    private int size;

    /**
     * 总页码
     */
    private int totalPage;

    /**
     * 总条数
     */
    private long total;

    /**
     * 数据
     */
    private List<T> data;

    public Page(int size, int totalPage, long total, List<T> data) {
        this.size = size;
        this.totalPage = totalPage;
        this.total = total;
        this.data = data;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
