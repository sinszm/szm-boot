package com.sinszm.es.support;

/**
 * 常量
 * @author sinszm
 */
public interface EsConstant {

    /**
     * 客户端实例名称
     */
    String CLIENT_BEAN = "esClient";
    String BUILDER_BEAN = "esBuilder";

    /**
     * 默认超时时长
     */
    long TIME_OUT = 10000;

}
