package com.sinszm.es.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Set;

/**
 * Es配置
 * @author sinszm
 */
@ConfigurationProperties(value = "szm.boot.es")
public class EsProperties {

    /**
     * 是否启用
     */
    private boolean enable;

    /**
     * ES节点，格式：IP:PORT
     */
    private Set<String> nodes;

    /**
     * 是否SSL模式
     */
    private boolean ssl;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public Set<String> getNodes() {
        return nodes;
    }

    public void setNodes(Set<String> nodes) {
        this.nodes = nodes;
    }

    public boolean isSsl() {
        return ssl;
    }

    public void setSsl(boolean ssl) {
        this.ssl = ssl;
    }
}
