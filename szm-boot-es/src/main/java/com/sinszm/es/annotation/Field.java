package com.sinszm.es.annotation;


import com.sinszm.es.annotation.support.FieldType;

import java.lang.annotation.*;

/**
 * 注解：ES字段索引
 *
 * @author sinszm
 */
@Target({ ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Field {

    String name() default "";

    FieldType type() default FieldType.Text;

    /**
     * 时间格式化，默认格式推荐：strict_date_time
     * <p>
     *     参照ES时间格式化文档.
     * </p>
     * @return  格式化
     */
    String format() default "";

    String searchAnalyzer() default "";

    String analyzer() default "";

    /**
     * 是否可以独立存储
     * @return  状态
     */
    boolean store() default false;

}
