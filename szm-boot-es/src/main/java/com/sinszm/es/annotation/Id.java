package com.sinszm.es.annotation;


import java.lang.annotation.*;

/**
 * 注解：标记ES字段索引主键
 *
 * @author sinszm
 */
@Target({ ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Id {
}
