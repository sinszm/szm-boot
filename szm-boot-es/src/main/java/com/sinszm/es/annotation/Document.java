package com.sinszm.es.annotation;


import java.lang.annotation.*;

/**
 * 注解：ES索引结构名称
 *
 * @author sinszm
 */
@Target({ ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Document {

    String value() default "";

}
