package com.sinszm.es.annotation.support;

/**
 * 字段类型
 * @author sinszm
 */
public enum FieldType {
    Text,
    Byte,
    Short,
    Integer,
    Long,
    Date,
    Half_Float,
    Float,
    Double,
    Boolean,
    Object,
    Nested,
    Ip,
    Attachment,
    Keyword
}
