package com.sinszm.wx.exception;

import com.sinszm.common.exception.ApiError;

/**
 * 微信开发错误代码
 *
 * @author chenjianbo
 */
public enum WxApiError implements ApiError {

    WX_PAY_CERT_NULL("微信支付证书异常."),

    WX_PAY_CERT_ERROR("微信支付证书读取异常."),

    WX_PAY_REG_FAIL("微信支付注册失败."),

    WX_PAY_ERROR_01("预约下单异常"),

    WX_PAY_ERROR_02("查询订单异常"),

    WX_PAY_ERROR_03("关闭订单异常"),

    WX_PAY_ERROR_04("申请退款异常"),

    WX_PAY_ERROR_05("退款查询异常"),

    WX_PAY_ERROR_06("对账查询异常"),

    WX_PAY_ERROR_07("结果通知签名验证错误"),

    WX_PAY_ERROR_08("结果通知签名异常"),

    ;

    private String message;

    WxApiError(String message) {
        this.message = message;
    }

    @Override
    public String getCode() {
        return this.name();
    }

    @Override
    public String getMessage() {
        return message;
    }

}
