package com.sinszm.wx.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 微信基础配置
 *
 * @author chenjianbo
 */
@ConfigurationProperties(value = "szm.boot.wx")
public class BasicWxProperties {

    /**
     * 公众号AppId
     */
    private String appId;

    /**
     * 支付主商户号
     * <p>
     *     关联AppId
     * </p>
     */
    private String mchId;

    /**
     * 主商户对应密钥
     */
    private String mchKey;

    /**
     * 主商户证书文件存储路径
     */
    private String mchCertUri;

    /**
     * 支付域名
     */
    private String mchDomain = "api.mch.weixin.qq.com";

    /**
     * 支付回调地址
     */
    private String payNotifyUrl;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getMchKey() {
        return mchKey;
    }

    public void setMchKey(String mchKey) {
        this.mchKey = mchKey;
    }

    public String getMchCertUri() {
        return mchCertUri;
    }

    public void setMchCertUri(String mchCertUri) {
        this.mchCertUri = mchCertUri;
    }

    public String getMchDomain() {
        return mchDomain;
    }

    public void setMchDomain(String mchDomain) {
        this.mchDomain = mchDomain;
    }

    public String getPayNotifyUrl() {
        return payNotifyUrl;
    }

    public void setPayNotifyUrl(String payNotifyUrl) {
        this.payNotifyUrl = payNotifyUrl;
    }
}
