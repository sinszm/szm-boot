package com.sinszm.wx.annotation;

import java.lang.annotation.*;

/**
 * 必填验证
 *
 * @author chenjianbo
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Required {
}
