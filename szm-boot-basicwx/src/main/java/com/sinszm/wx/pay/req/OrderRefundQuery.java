package com.sinszm.wx.pay.req;

import com.sinszm.wx.annotation.Required;

/**
 * 退款查询订单参数
 *
 * @author chenjianbo
 */
public class OrderRefundQuery extends AbstractAppId {

    /**
     * 子商户公众账号ID
     */
    private String subAppid;

    /**
     * 子商户号
     */
    private String subMchId;

    /**
     * 商户退款单号
     */
    @Required
    private String outRefundNo;

    public String getSubAppid() {
        return subAppid;
    }

    public void setSubAppid(String subAppid) {
        this.subAppid = subAppid;
    }

    public String getSubMchId() {
        return subMchId;
    }

    public void setSubMchId(String subMchId) {
        this.subMchId = subMchId;
    }

    public String getOutRefundNo() {
        return outRefundNo;
    }

    public void setOutRefundNo(String outRefundNo) {
        this.outRefundNo = outRefundNo;
    }
}
