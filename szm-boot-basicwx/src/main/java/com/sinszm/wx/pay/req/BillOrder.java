package com.sinszm.wx.pay.req;

import com.sinszm.wx.annotation.Required;

/**
 * 对账订单参数
 *
 * @author chenjianbo
 */
public class BillOrder extends AbstractAppId {

    /**
     * 子商户公众账号ID
     */
    private String subAppid;

    /**
     * 子商户号
     */
    private String subMchId;

    /**
     * 对账单日期
     */
    @Required
    private String billDate;

    /**
     * 对账类型
     */
    private String billType;

    public String getSubAppid() {
        return subAppid;
    }

    public void setSubAppid(String subAppid) {
        this.subAppid = subAppid;
    }

    public String getSubMchId() {
        return subMchId;
    }

    public void setSubMchId(String subMchId) {
        this.subMchId = subMchId;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }
}
