package com.sinszm.wx.pay.req;

import com.sinszm.wx.annotation.Required;
import org.nutz.json.Json;
import org.nutz.json.JsonFormat;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 统一下单参数
 *
 * @author chenjianbo
 */
public class UnifiedOrder extends AbstractAppId {

    /**
     * 用户标识
     */
    private String openid;

    /**
     * 子商户公众账号ID
     * 可为空
     * <p>
     *     微信分配的子商户公众账号ID，如需在支付完成后获取sub_openid则此参数必传。
     * </p>
     */
    private String subAppid;

    /**
     * 用户子标识
     */
    private String subOpenid;

    /**
     * 子商户号
     * 可为空
     * <p>
     *     微信支付分配的子商户号
     * </p>
     */
    private String subMchId;

    /**
     * 设备号
     */
    private String deviceInfo;

    /**
     * 开发票入口开放标识
     */
    private String receipt = "Y";

    /**
     * 商品描述
     */
    @Required
    private String body;

    /**
     * 商品详情
     */
    private String detail;

    /**
     * 附加数据
     * <p>
     *     自定义数据
     * </p>
     */
    private String attach;

    /**
     * 商户订单号
     */
    @Required
    private String outTradeNo;

    /**
     * 货币类型
     */
    private String feeType = "CNY";

    /**
     * 总金额
     */
    @Required
    private Integer totalFee;

    /**
     * 终端IP
     */
    @Required
    private String spbillCreateIp;

    /**
     * 交易起始时间
     */
    private String timeStart;

    /**
     * 交易结束时间
     */
    private String timeExpire;

    /**
     * 订单优惠标记
     */
    private String goodsTag;

    /**
     * 通知地址
     */
    @Required
    private String notifyUrl;

    /**
     * 交易类型
     */
    @Required
    private String tradeType;

    /**
     * 商品ID
     */
    private String productId;

    /**
     * 指定支付方式
     */
    private String limitPay;

    /**
     * 场景信息
     */
    private String sceneInfo;

    /**
     * 设置门店信息
     * @param id        门店ID
     * @param name      门店名称
     * @param areaCode  门店区域代码
     * @param address   门店地址
     * @return          Json的字符串门店信息
     */
    public String putSceneInfo(String id, String name, String areaCode, String address) {
        Map<String,String> storeInfo = new HashMap<>(0);
        storeInfo.put("id", StringUtils.isEmpty(id) ? "" : id);
        storeInfo.put("name", StringUtils.isEmpty(name) ? "" : name);
        storeInfo.put("area_code", StringUtils.isEmpty(areaCode) ? "" : areaCode);
        storeInfo.put("address", StringUtils.isEmpty(address) ? "" : address);
        Map<String, String> map = new HashMap<>(0);
        map.put("store_info", Json.toJson(storeInfo, JsonFormat.tidy()));
        this.sceneInfo = Json.toJson(map, JsonFormat.tidy());
        return this.sceneInfo;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getSubAppid() {
        return subAppid;
    }

    public void setSubAppid(String subAppid) {
        this.subAppid = subAppid;
    }

    public String getSubOpenid() {
        return subOpenid;
    }

    public void setSubOpenid(String subOpenid) {
        this.subOpenid = subOpenid;
    }

    public String getSubMchId() {
        return subMchId;
    }

    public void setSubMchId(String subMchId) {
        this.subMchId = subMchId;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public Integer getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(Integer totalFee) {
        this.totalFee = totalFee;
    }

    public String getSpbillCreateIp() {
        return spbillCreateIp;
    }

    public void setSpbillCreateIp(String spbillCreateIp) {
        this.spbillCreateIp = spbillCreateIp;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeExpire() {
        return timeExpire;
    }

    public void setTimeExpire(String timeExpire) {
        this.timeExpire = timeExpire;
    }

    public String getGoodsTag() {
        return goodsTag;
    }

    public void setGoodsTag(String goodsTag) {
        this.goodsTag = goodsTag;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getLimitPay() {
        return limitPay;
    }

    public void setLimitPay(String limitPay) {
        this.limitPay = limitPay;
    }

    public String getSceneInfo() {
        return sceneInfo;
    }

    public void setSceneInfo(String sceneInfo) {
        this.sceneInfo = sceneInfo;
    }
}