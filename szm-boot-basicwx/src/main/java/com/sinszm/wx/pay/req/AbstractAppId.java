package com.sinszm.wx.pay.req;

/**
 * 抽象提取的公共参数
 *
 * @author sinszm
 */
public abstract class AbstractAppId {

    private String appid;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }
}
