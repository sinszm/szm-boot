package com.sinszm.wx.pay.req;

import com.sinszm.wx.annotation.Required;

/**
 * 申请退款参数
 *
 * @author chenjianbo
 */
public class RefundOrder extends AbstractAppId {

    /**
     * 子商户公众账号ID
     * 可为空
     * <p>
     *     微信分配的子商户公众账号ID，如需在支付完成后获取sub_openid则此参数必传。
     * </p>
     */
    private String subAppid;

    /**
     * 子商户号
     * 可为空
     * <p>
     *     微信支付分配的子商户号
     * </p>
     */
    private String subMchId;

    /**
     * 商户订单号
     */
    private String outTradeNo;

    /**
     * 微信订单号
     */
    private String transactionId;

    /**
     * 商户退款单号
     */
    @Required
    private String outRefundNo;

    /**
     * 总金额
     */
    @Required
    private Integer totalFee;

    /**
     * 申请退款金额
     */
    @Required
    private Integer refundFee;

    /**
     * 退款货币种类
     */
    private String refundFeeType = "CNY";
    /**
     * 退款原因
     */
    private String refundDesc;

    /**
     * 退款资金来源
     */
    private String refundAccount = "REFUND_SOURCE_RECHARGE_FUNDS";

    /**
     * 退款通知地址
     */
    private String notifyUrl;

    public String getSubAppid() {
        return subAppid;
    }

    public void setSubAppid(String subAppid) {
        this.subAppid = subAppid;
    }

    public String getSubMchId() {
        return subMchId;
    }

    public void setSubMchId(String subMchId) {
        this.subMchId = subMchId;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getOutRefundNo() {
        return outRefundNo;
    }

    public void setOutRefundNo(String outRefundNo) {
        this.outRefundNo = outRefundNo;
    }

    public Integer getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(Integer totalFee) {
        this.totalFee = totalFee;
    }

    public Integer getRefundFee() {
        return refundFee;
    }

    public void setRefundFee(Integer refundFee) {
        this.refundFee = refundFee;
    }

    public String getRefundFeeType() {
        return refundFeeType;
    }

    public void setRefundFeeType(String refundFeeType) {
        this.refundFeeType = refundFeeType;
    }

    public String getRefundDesc() {
        return refundDesc;
    }

    public void setRefundDesc(String refundDesc) {
        this.refundDesc = refundDesc;
    }

    public String getRefundAccount() {
        return refundAccount;
    }

    public void setRefundAccount(String refundAccount) {
        this.refundAccount = refundAccount;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }
}